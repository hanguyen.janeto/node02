var express = require('express');
var app = express();
var fs = require("fs");

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Phuong thuc get() phan hoi mot GET Request ve Homepage
app.get('/user/:id', function (req, res) {
    var id = req.params.id;
    fs.readFile(__dirname + "/user.json", "utf8", function (err, data) {
        if (err) {
            res.status(404);
            res.send(err.message);
        } else {
            var formattedData = JSON.parse(data);
            for (var i = 0; i < formattedData.length; i++) {
                if (formattedData[i].id == id) {
                    res.send(formattedData[i]);
                    return;
                }
            }

            res.status(404);
            res.send("Not Found");
        }
    })
})

// Phuong thuc post() phan hoi mot POST Request ve Homepage
app.post('/user', function (req, res) {
    var user = req.body;
    console.log(user);

    if(!user.username) {
        res.status(400);
        res.end("Username is required");
    }

    fs.readFile(__dirname + "/user.json", "utf8", function (err, data) {
        if (err) {
            res.status(404);
            res.send(err.message);
        } else {
            var formattedData = JSON.parse(data);
            formattedData.push(user);

            fs.writeFile(__dirname + "/user.json", JSON.stringify(formattedData), function (err) {
                if (err) {
                    res.status(500);
                    res.send(err.message);
                } else {
                    res.send(user);
                }
            })
        }
    })
})

// Phuong thuc delete() phan hoi mot DELETE Request ve /del_user page.
app.delete('/user/:id', function (req, res) {
    var id = req.params.id;
    console.log("Nhan mot DELETE Request ve /del_user");
    res.send('Hello DELETE ' + id);
})

// Phuong thuc nay phan hoi mot GET Request ve /list_user page.
// app.get('/user', function (req, res) {
//     fs.readFile(__dirname + "/user.json", "utf8", function (err, data) {
//         if (err) {
//             res.status(404);
//             res.send(err.message);
//         } else {
//             var formattedData = JSON.parse(data);
//             res.send(formattedData);
//         }
//     })
// })

app.use('/user', require('./routes/user.route'));

var server = app.listen(8081, function () {
    console.log("Ung dung Node.js dang lang nghe tai dia chi: ")
})