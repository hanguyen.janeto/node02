var events = require('events');

var eventEmitter = new events.EventEmitter();

eventEmitter.on('hello', function() {
    console.log('on Hello');
});

eventEmitter.emit('hello');