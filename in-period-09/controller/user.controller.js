var fs = require('fs');
var path = require('path');
var userDao = require('./../dao/user.dao');

module.exports = {
    getUsers: getUsers
}

function getUsers(req, res, next) {
    // return new Promise(function (resolve, reject) {
    //     fs.readFile(path.join(__dirname, "../" + "users.json"), 'utf8', function (err, data) {
    //         if (err) {
    //             reject({
    //                 message: err.message
    //             });
    //         } else {
    //             resolve(data);
    //         }
    //     });
    // })
    new Promise()
    var query = req.query;
    var page = query.page;
    var limit = query.limit;
    var skip = page > 1 ? (page - 1) * limit : 0;
    var sortBy = query.sortBy;
    var isDes = query.isDes ? true : false;
    userDao.findAll({}, skip, limit, "", sortBy, isDes)
        .then(function (users) {
                res.send(users);
            }

        )
        .catch(function (err) {
            next(err);
        })
}

function createUser(newUser) {
    userDao.create()
}