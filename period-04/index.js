var express = require('express');
var app = express();

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Phuong thuc get() phan hoi mot GET Request ve Homepage
app.get('/user/:xuay', function (req, res) {
    var idxxx = req.params.xuay;
    console.log(idxxx);
    console.log("Nhan mot GET Request ve Homepage");
    res.send('Hello GET ' + idxxx);
})

// Phuong thuc post() phan hoi mot POST Request ve Homepage
app.post('/user', function (req, res) {
    console.log(req.body);
    // console.log(user);
    console.log("Nhan mot POST Request ve Homepage");
    res.send(req.body);
})

// Phuong thuc delete() phan hoi mot DELETE Request ve /del_user page.
app.delete('/user/:id', function (req, res) {
    var id = req.params.id;
    console.log("Nhan mot DELETE Request ve /del_user");
    res.send('Hello DELETE ' + id);
})

// Phuong thuc nay phan hoi mot GET Request ve /list_user page.
app.get('/user', function (req, res) {
    console.log("Nhan mot GET Request ve /list_user");
    res.send('Page Listing');
})

var server = app.listen(8081, function () {
    console.log("Ung dung Node.js dang lang nghe tai dia chi: ")
})